class RPNCalculator

  def initialize
    @calculator = []
  end

  def push(n)
    @calculator << n
  end

  def plus
    if @calculator.size < 2
      raise "calculator is empty"
    else
      operand = []
      2.times { operand << @calculator.pop }
      @calculator << operand.reverse.reduce(:+)
    end
  end

  def minus
    if @calculator.size < 2
      raise "calculator is empty"
    else
      operand = []
      2.times { operand << @calculator.pop }
      @calculator << operand.reverse.reduce(:-)
    end
  end

  def divide
    if @calculator.size < 2
      raise "calculator is empty"
    else
      operand = []
      2.times { operand << @calculator.pop.to_f }
      @calculator << operand.reverse.reduce(:/)
    end
  end

  def times
    if @calculator.size < 2
      raise "calculator is empty"
    else
      operand = []
      2.times { operand << @calculator.pop }
      @calculator << operand.reverse.reduce(:*)
    end
  end

  def value
    @calculator.last
  end

  def tokens(str)
    toks = []
    str.split.each do |ch|
      if ch.to_i != 0
        toks << ch.to_i
      else
        case ch
        when "+"
          toks << :+
        when "-"
          toks << :-
        when "/"
          toks << :/
        when "*"
          toks << :*
        end
      end
    end
    toks
  end

  def evaluate(str)
    str.split.each do |ch|
      if ch.to_i != 0
        @calculator.push(ch.to_i)
      else
        case ch
        when "+"
          self.plus
        when "-"
          self.minus
        when "/"
          self.divide
        when "*"
          self.times
        end
      end
    end
    self.value
  end

end
